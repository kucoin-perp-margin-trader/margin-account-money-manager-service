const axios = require('axios')

const path = 'http://localhost:5000/margin-ema-trading-bot/'

const fetchEMASignals = async(symbols, granularities) => {
    const data = await axios.get(path, {
        params: {
          symbols,
          granularities
        }
      })
      .then(res => {
        return res.data
      })
      .catch(err => {
        console.error(err)
      })
    return data
}

module.exports = {
    fetchEMASignals
}