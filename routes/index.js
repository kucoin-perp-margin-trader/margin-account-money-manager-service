var express = require('express');
var router = express.Router();
const axios = require('axios')
const {v4: uuidv4} = require('uuid')

const config = require('../kucoin-config')

const { fetchEMASignals } = require('./fetch_ema_bot_signals')
const { fetchPastOrderData, fetchUSDTBalance } = require('./fetch_kucoin_data')
const { futures_currencies_limits } = require('./futures_currencies_limits');

const main = async() => {
  //const tickerinfo = await fetchTickerInfo()
  const past_order_data = await fetchPastOrderData()
  const closed_orders = past_order_data.filter(order => order.isOpen==false)
  const open_orders = past_order_data.filter(order => order.isOpen==true)
  //console.log(open_orders)
  const symbols_in_current_portfolio = open_orders.map(order => {return order.symbol})
  //GET THESE AND SEND THEM TO FRONTEND OF OPEN AND CLOSED ORDERS: symbol, currentQty, currentCost, realLeverage, markValue, avgEntryPrice, markPrice, realizedPnl, unrealizedPnl, unrealisedRoePcnt (%gain) 
  const new_symbols = ['FILUSDTM', 'ETHUSDTM', 'XBTUSDTM', 'ADAUSDTM', 'XRPUSDTM', 'VETUSDTM', ]
  //fetch signals for everything that already is in your portfolio, + possibly other assets (like FIL in this case)
  const symbols = Array.from(new Set(symbols_in_current_portfolio.concat(new_symbols)))
  //divide sothat the signals for 5 min run all the time BUT ONLY EXECUTE ONCE PER PERIOD (the signal for 5min comes in 5x for instance!!)
  const granularities = [1, 5]
  const ema_signals = await fetchEMASignals(symbols, granularities)
  console.log(ema_signals)
  //console.log(ema_signals)
  //todo only fetch sell signals TO CANCEL ORDERS for things currently in portfolio, but short signals for whatever the user wants
  const canceled_positions = await cancelOpenPositions(open_orders, ema_signals)
  const ema_trade_signals = ema_signals.filter(signal => signal.ema_signal!=1)
  //console.log(ema_trade_signals)
  const traded_assets = await tradeAssets(ema_trade_signals)
  //console.log(traded_assets)
}

const fetchTickerInfo = async() => {
  //get ticker info  mostly needed when adding new assets, isnt part of the workflow
  axios.get(`${config.baseUrl}/api/v1/contracts/active`)
       .then(res => {
         console.log(res.data)
       })  
       .catch(err => {
         console.error(err)
       })  
}

const hmacSHA256 = require('crypto-js/hmac-sha256')
const base64 = require('crypto-js/enc-base64');

//todo this doesnt seem to be working properly
const cancelOpenPositions = async(open_orders, signals) => {
  if(signals.length==0) return console.log("No trading signals received")
  const req_path = '/api/v1/orders'
  const responses = []
  const signals_in_port = open_orders.filter(order => {
    return signals.some(signal => {
      return (signal.symbol === order.symbol) && (order.currentQty<0 && (signal.ema_signal==2) || (order.currentQty>0 && signal.ema_signal==0))
    })
  })
  if(signals_in_port.length==0) return console.log("No trading signals in your portfolio")
  for(signal of signals_in_port) {
    const side = signal.currentQty>0 ? 'sell' : 'buy' 
    const leverage = signal.realLeverage
    const size = -signal.currentQty

    const timestamp = Date.now()
    const method = 'POST'
    const body = {
      clientOid: uuidv4(),
      side,
      symbol: signal.symbol,
      type: 'market',
      size,
      leverage
    }
    const what = timestamp+method+req_path+JSON.stringify(body)
    const sign = base64.stringify(hmacSHA256(what, process.env.secret))

    await axios({
      method: 'POST',
      url: `${config.baseUrl}${req_path}`,
      headers: {
        'KC-API-KEY': process.env.key,
        'KC-API-SIGN': sign,
        'KC-API-TIMESTAMP': timestamp,
        'KC-API-PASSPHRASE': process.env.passphrase
      },
      data: body
    }).then(res => {
      responses.push(res.data)
      return
    }).catch(err => {
      console.error(err)
    })
  }
  return responses
}

const tradeAssets = async(trade_signals) => {
  if(trade_signals.length==0) return console.log("No trading signals received")
  const req_path = '/api/v1/orders'
  const responses = []
  
  for(signal of trade_signals) {

    const balance_data = await fetchUSDTBalance()
    const available_balance = balance_data.available_balance
    //10% of whole portfolio per trade
    const usdt_size_per_order = balance_data.accountEquity / 10

    if(usdt_size_per_order>available_balance) {
      console.log('Insufficient balance for trading assets.')
      break
    }

    const side = signal.ema_signal==0 ? 'sell' : 'buy' 
    const leverage = signal.granularity=='1' ? 15 : 30
    const curr_info = futures_currencies_limits.find(obj => {
      //find the ticker, remove the USDTM at the end
      return obj.ticker == signal.symbol.slice(0, -5)
    })
    //dodge the floating point err by multiplying and then dividing, ceil to the nearest higher number
    const size = parseFloat(Math.ceil((usdt_size_per_order * leverage / signal.lastPrice)*(1/curr_info.limit))/(1/curr_info.limit))/curr_info.limit

    const timestamp = Date.now()
    const method = 'POST'
    const body = {
      clientOid: uuidv4(),
      side,
      symbol: signal.symbol,
      type: 'market',
      size,
      leverage
    }
    const what = timestamp+method+req_path+JSON.stringify(body)
    const sign = base64.stringify(hmacSHA256(what, process.env.secret))

    await axios({
      method: 'POST',
      url: `${config.baseUrl}${req_path}`,
      headers: {
        'KC-API-KEY': process.env.key,
        'KC-API-SIGN': sign,
        'KC-API-TIMESTAMP': timestamp,
        'KC-API-PASSPHRASE': process.env.passphrase
      },
      data: body
    }).then(res => {
      console.log(res)
      responses.push(res.data)
      return
    }).catch(err => {
      console.error(err)
    })
    var d = new Date()
    console.log(`New order has been placed.
                 Date: ${d.getDate()}.${d.getMonth()+1}.${d.getFullYear()} 
                 Time: ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()} 
                 Trading pair: ${signal.symbol}, 
                 Side: ${side}, 
                 Size: ${size}, 
                 Leverage: ${leverage}x`)
  }
  return responses
}

//run every minute
setInterval(main, 1 * 60 * 1000)

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
