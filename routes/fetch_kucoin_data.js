const axios = require('axios')
const baseUrl = require('../kucoin-config').baseUrl
const hmacSHA256 = require('crypto-js/hmac-sha256')
const base64 = require('crypto-js/enc-base64')

const fetchPastOrderData = async() => {
    const timestamp = Date.now()
    const method = 'GET'
    const req_path = '/api/v1/positions'
    const body = ''
    const what = timestamp+method+req_path+body
    const sign = base64.stringify(hmacSHA256(what, process.env.secret))
    const res = axios.get(`${baseUrl}${req_path}`, {
                        headers: {
                            'KC-API-KEY': process.env.key,
                            'KC-API-SIGN': sign,
                            'KC-API-TIMESTAMP': timestamp,
                            'KC-API-PASSPHRASE': process.env.passphrase
                        }
                     })
                     .then(res => {
                         return res.data.data
                     })
                     .catch(err => {
                         console.error(err)
                     })
    return res                 
}

const fetchUSDTBalance = async() => {
    const timestamp = Date.now()
    const method = 'GET'
    const req_path = '/api/v1/account-overview?currency=USDT'
    const body = ''
    const what = timestamp+method+req_path+body
    const sign = base64.stringify(hmacSHA256(what, process.env.secret))
    const res = axios.get(`${baseUrl}${req_path}`, {
                        headers: {
                            'KC-API-KEY': process.env.key,
                            'KC-API-SIGN': sign,
                            'KC-API-TIMESTAMP': timestamp,
                            'KC-API-PASSPHRASE': process.env.passphrase
                        }
                     })
                     .then(res => {
                         return res.data.data
                     })
                     .catch(err => {
                         console.error(err)
                     })
    return res         
  }

module.exports = {
    fetchPastOrderData,
    fetchUSDTBalance
}