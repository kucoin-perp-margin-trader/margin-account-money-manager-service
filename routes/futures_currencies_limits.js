const futures_currencies_limits = [
    {
        ticker: "XBT",
        limit: 0.001
    },
    {
        ticker: "ETH",
        limit: 0.01
    },
    {
        ticker: "BCH",
        limit: 0.01
    },
    {
        ticker: "BSV",
        limit: 0.01
    },
    {
        ticker: "LINK",
        limit: 0.1
    },
    {
        ticker: "UNI",
        limit: 1
    },
    {
        ticker: "YFI",
        limit: 0.0001
    },
    {
        ticker: "EOS",
        limit: 1
    },
    {
        ticker: "DOT",
        limit: 1
    },
    {
        ticker: "FIL",
        limit: 0.1
    },
    {
        ticker: "ADA",
        limit: 10
    },
    {
        ticker: "XRP",
        limit: 10
    },
    {
        ticker: "LTC",
        limit: 0.1
    },
    {
        ticker: "TRX",
        limit: 100
    },
    {
        ticker: "GRT",
        limit: 1
    },
    {
        ticker: "SUSHI",
        limit: 1
    },
    {
        ticker: "XLM",
        limit: 10
    },
    {
        ticker: "1INCH",
        limit: 1
    },
    {
        ticker: "ZEC",
        limit: 0.01
    },
    {
        ticker: "DASH",
        limit: 0.01
    },
    {
        ticker: "AAVE",
        limit: 0.01
    },
    {
        ticker: "KSM",
        limit: 0.01
    },
    {
        ticker: "DOGE",
        limit: 100
    },
    {
        ticker: "LUNA",
        limit: 1
    },
    {
        ticker: "VET",
        limit: 100
    },
    {
        ticker: "BNB",
        limit: 0.01
    },
    {
        ticker: "SXP",
        limit: 1
    },
    {
        ticker: "SOL",
        limit: 0.1
    },
    {
        ticker: "IOST",
        limit: 100
    },
    {
        ticker: "CRV",
        limit: 1
    },
    {
        ticker: "ALGO",
        limit: 1
    },
    {
        ticker: "AVAX",
        limit: 0.1
    },
    {
        ticker: "FTM",
        limit: 1
    },
    {
        ticker: "MATIC",
        limit: 10
    },
    {
        ticker: "THETA",
        limit: 0.1
    },
    {
        ticker: "ATOM",
        limit: 0.1
    },
    {
        ticker: "BTT",
        limit: 1000
    },
    {
        ticker: "CHZ",
        limit: 1
    },
  ]

exports.futures_currencies_limits = futures_currencies_limits;